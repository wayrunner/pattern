import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;

import weka.core.Instances;

public class OperaMain {

	public static void main(String[] args) throws Exception {
		boolean hasGroundTruth = true;
//		File rawFile = new File("t01_to_t08.arff");
		 File rawFile = new File("opera/t08.arff");
		// File rawFile = new File("t01_to_t08.arff");
//		File rawFile = new File("opera/t04.arff");
		System.out.println("Start");
		Instances[] trainInstances = LoaderUtils.loadTrainInstances();
		InstanceReducer[] instanceReducers = new InstanceReducer[3];
		instanceReducers[MainOperaClassifier.INDEX_MALE] = new AttributeInstanceReducer(
				trainInstances[MainOperaClassifier.INDEX_MALE]);
		instanceReducers[MainOperaClassifier.INDEX_FEMALE] = new AttributeInstanceReducer(
				trainInstances[MainOperaClassifier.INDEX_FEMALE]);
		instanceReducers[MainOperaClassifier.INDEX_CHORUS] = new AttributeInstanceReducer(
				trainInstances[MainOperaClassifier.INDEX_CHORUS]);

		OperaClassifier[][] reducedClassifiers = LoaderUtils.loadClassifiers();
		MainOperaClassifier clazzifier = new MainOperaClassifier(
				instanceReducers, reducedClassifiers,
				LoaderUtils.loadAssembleClassifiers(),
				LoaderUtils.loadSmoothingClassifier());
		System.out.println("Loaded classifiers");

		File[] assembleDataSetFiles = new File[] {
				new File("assambleDataset0.arff"),
				new File("assambleDataset1.arff"),
				new File("assambleDataset2.arff") };
		Instances[] assambleData;
		Instances multiassambleData;
		Instances gt = null;
		if (assembleDataSetFiles[0].exists() && false) {
			assambleData = new Instances[assembleDataSetFiles.length];
			for (int i = 0; i < assambleData.length; i++) {
				assambleData[i] = LoaderUtils
						.loadInstancesFile("assambleDataset" + i + ".arff");
				assambleData[i]
						.setClassIndex(assambleData[i].numAttributes() - 1);
			}
			clazzifier.setAssambleDataSets(assambleData);
			System.out.println("Loaded assemble data");
			gt = new Instances(LoaderUtils.loadInstancesFile("test.arff"));
		} else {
			Instances rawData = LoaderUtils
					.loadInstancesFile(rawFile.getPath());
			if (hasGroundTruth) {
				System.out.println("has ground truth");
				gt = new Instances(rawData);
				rawData.deleteAttributeAt(rawData.numAttributes() - 1);
				rawData.deleteAttributeAt(rawData.numAttributes() - 1);
				rawData.deleteAttributeAt(rawData.numAttributes() - 1);
			}
			System.out.println("loaded test data");
			// Classifier[] assambleClassifiers = LoaderUtils
			// .loadAssembleClassifiers();
			// Classifier smoothingClassifier =
			// LoaderUtils.loadSmoothingClassifier();

			Instances[] reducedData = clazzifier.reduceDataSets(rawData);
			System.out.println("reduceDataSets");
			assambleData = clazzifier.generateAssambleDataset();
			multiassambleData = clazzifier
					.generateMultiAssambleDataset(assambleData);
			System.out.println("generateAssambleDataset");
			for (int i = 0; i < assambleData.length; i++) {
				if (gt != null) {
					LoaderUtils.setGroundTruth(assambleData[i], gt,
							gt.numAttributes() - i - 1);
				}

				LoaderUtils.saveDataSet(assambleData[i],
						assembleDataSetFiles[i]);
			}

			LoaderUtils.setMultiGroundTruth(multiassambleData, gt);
			LoaderUtils.saveDataSet(multiassambleData, new File(
					"multidataset.arff"));
			System.out.println("saved assamble");
		}

		System.out.println("generated assamble");
		double[][] assambleResults = clazzifier.generateAssambleResults();
		System.out.println("generateAssambleResults");
		Instances[] smoothingDataset = clazzifier.generateSmoothingDataset();
		for (int i = 0; i < smoothingDataset.length; i++) {
			if (gt != null)
				LoaderUtils.setGroundTruth(smoothingDataset[i], gt,
						gt.numAttributes() - 1 - i);
			LoaderUtils.saveDataSet(smoothingDataset[i], new File("smooth_" + i
					+ ".arff"));
		}
		System.out.println("saved smoothing dataset");
		double[][] finalResult = clazzifier.generateFinalResults();
		System.out.println("generated final result");

		saveFinalResult(rawFile, finalResult);
		if (gt != null) {
			int[][][] errors = new int[2][2][2];
			for (int singer = 0; singer < finalResult[0].length; singer++) {
				int singerAtributeIndex = gt.numAttributes() - 1 - singer;
				for (int i = 0; i < finalResult.length; i++) {
					int result = (int) Math.round(finalResult[i][singer]);
					result = result == 0 ? 1 : 0;
					int truth = (int) gt.instance(i).value(singerAtributeIndex);
					errors[singer][result][truth]++;
				}
			}
			for (int i = 0; i < finalResult[0].length; i++) {
				float s = errors[i][0][0] + errors[i][1][1];
				float e = errors[i][1][0] + errors[i][0][1];
				System.out.println("Class " + i + ": "
						+ Arrays.deepToString(errors[i])
						+ String.format(" %.2f", s * 100 / (s + e)) + "%");
			}
			printCosts(finalResult, gt, smoothingDataset, clazzifier);
		}
		// System.out.println(smoothingDataset);
		// clazzifier.classify(rawData);

	}

	private static void printCosts(double[][] finalResult,
			Instances groundTruth, Instances[] smoothingDataSet,
			MainOperaClassifier mainClassifier) throws Exception {
		if (groundTruth != null) {
			int indexMale = groundTruth.numAttributes() - 1
					- MainOperaClassifier.INDEX_MALE;
			int indexFemale = groundTruth.numAttributes() - 1
					- MainOperaClassifier.INDEX_FEMALE;
			int totalGain = 0;
			int maxGain = 0;
			int truthBothButPredictedOne = 0;
			int truthOneButPredictedBoth = 0;
			int predictClassWrong = 0;
			int correctPredict = 0;
			for (int sample = 0; sample < finalResult.length; sample++) {
				int maleTruth = (int) groundTruth.instance(sample).value(
						indexMale);
				int femaleTruth = (int) groundTruth.instance(sample).value(
						indexFemale);

				double[] predictionsMale = mainClassifier
						.getSmoothingClassifier()
						.distributionForInstance(
								smoothingDataSet[MainOperaClassifier.INDEX_MALE]
										.instance(sample));
				double[] predictionsFemale = mainClassifier
						.getSmoothingClassifier()
						.distributionForInstance(
								smoothingDataSet[MainOperaClassifier.INDEX_FEMALE]
										.instance(sample));
				double maleP = predictionsMale[0];
				double femaleP = predictionsFemale[0];
				int maleResult = getMaleResult(maleP, femaleP);
				maleResult = maleResult == 0 ? 1 : 0;
				int femaleResult = getFemaleResult(maleP, femaleP);
				femaleResult = femaleResult == 0 ? 1 : 0;
				// we wouldn't need "totalGain", but to check calculation we
				// just leave it there..
				// check actual gain
				if (maleResult == 1 && femaleResult == 1) {
					if (maleTruth == 1 && femaleTruth == 1) {
						totalGain += 2;
						correctPredict++;
					} else if (maleTruth == 1 || femaleTruth == 1) {
						totalGain += -1;
						truthOneButPredictedBoth++;
					} else {
						predictClassWrong++;
						totalGain += -2;
					}
				} else if (maleResult == 1) {
					if (maleTruth == 1 && femaleTruth == 1) {
						totalGain += 1;
						truthBothButPredictedOne++;
					} else if (maleTruth == 0) {
						totalGain += -2;
						predictClassWrong++;
					} else {
						totalGain += 2; // male truth = 1
						correctPredict++;
					}
				} else if (femaleResult == 1) {
					if (maleTruth == 1 && femaleTruth == 1) {
						totalGain += 1;
						truthBothButPredictedOne++;
					} else if (femaleTruth == 0) {
						totalGain += -2;
						predictClassWrong++;
					} else {
						totalGain += 2; // female truth = 1
						correctPredict++;
					}
				}

				if (maleTruth == 1 || femaleTruth == 1) {
					maxGain += 2;
				}
			}
			System.out.println(totalGain * 100.0 / maxGain
					+ "% GAIN : Total gain: " + totalGain + ", Maximum Gain: "
					+ maxGain);
			System.out.println("\t true: both, predict: one true (cost: +1): "
					+ truthBothButPredictedOne + ", = "
					+ truthBothButPredictedOne + " cost");
			System.out.println("\t true: one, predict: both true (cost: -1): "
					+ truthOneButPredictedBoth + ", = "
					+ truthOneButPredictedBoth * -1 + " cost");
			System.out.println("\t class wrongly predicted: (cost: -2): "
					+ predictClassWrong + ", = " + predictClassWrong * -2
					+ " cost");
			System.out.println("\t correct predictions: (cost: +2): "
					+ correctPredict + ", = " + correctPredict * 2 + " cost");
		}
	}

	private static int getMaleResult(double maleP, double femaleP) {
//		if (maleP >= 0.5)
//			return 1;
//		if (femaleP >= 0.5)
//			return 0;
//		if (maleP + femaleP > 0.8)
//			return 1;
//		return 0;
		 return (int) Math.round(maleP);
	}

	private static int getFemaleResult(double maleP, double femaleP) {
//		if (femaleP >= 0.5)
//			return 1;
//		if (maleP >= 0.5)
//			return 0;
//		if (maleP + femaleP > 0.8)
//			return 1;
//		return 0;
		 return (int) Math.round(femaleP);
	}

	private static void saveFinalResult(File rawFile, double[][] finalResult)
			throws IOException {
		FileWriter fFemale = new FileWriter(rawFile.getName() + "_female.txt");
		FileWriter fMale = new FileWriter(rawFile.getName() + "_male.txt");
		for (int i = 0; i < finalResult.length; i++) {
			fFemale.append(Math
					.round(finalResult[i][MainOperaClassifier.INDEX_FEMALE])
					+ "\n");
			fMale.append(Math
					.round(finalResult[i][MainOperaClassifier.INDEX_MALE])
					+ "\n");
		}
		fMale.flush();
		fFemale.flush();
	}
}