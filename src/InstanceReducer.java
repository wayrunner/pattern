import weka.core.Instance;
import weka.core.Instances;

public interface InstanceReducer {
	public Instances reduce(Instances rawData) throws Exception;
}
