import java.util.ArrayList;
import java.util.List;

import weka.classifiers.Classifier;
import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.ProtectedProperties;

public class MainOperaClassifier {
	public static final int INDEX_MALE = 0;
	public static final int INDEX_CHORUS = 2;
	public static final int INDEX_FEMALE = 1;
	private Classifier[] assambleClassifiers;
	private double[][] assambleResults;
	private InstanceReducer[] instanceReducers;
	private OperaClassifier[][] reducedClassifiers;
	private int SMOOTHING_WINDOW_SIZE_FUTURE = 10;
	private int SMOOTHING_WINDOW_SIZE_PAST = 10;
	private Classifier smoothingClassifier;
	private Instances[] smoothingDataSets;
	private Instances[] reducedDatasets;
	private Instances[] assambleDataSets;

	public MainOperaClassifier(InstanceReducer[] instanceReducers,
			OperaClassifier[][] reducedClassifiers,
			Classifier[] assambleClassifiers, Classifier smoothingClassifier) {
		this.instanceReducers = instanceReducers;
		this.reducedClassifiers = reducedClassifiers;
		this.assambleClassifiers = assambleClassifiers;
		this.smoothingClassifier = smoothingClassifier;
	}

	public MainOperaClassifier() {
	}

	public double[][] classify(Instances rawData) throws Exception {
		Instances[] reducedDatasets = reduceDatasets(rawData, instanceReducers);
		Instances[] assambleDataSets = generateAssambleDatasets(
				reducedDatasets, reducedClassifiers);
		double[][] assambleResult = generateAssambleResults(assambleDataSets,
				assambleClassifiers);
		Instances[] smoothingDataSets = generateSmoothingDataset(assambleResult);
		return generateFinalResults(smoothingDataSets, smoothingClassifier);
	}

	public Instances[] generateAssambleDataset() throws Exception {
		return generateAssambleDatasets(reducedDatasets, reducedClassifiers);
	}

	public double[][] generateAssambleResults() throws Exception {
		return generateAssambleResults(assambleDataSets, assambleClassifiers);
	}

	public Instances[] generateSmoothingDataset() {
		return generateSmoothingDataset(assambleResults);
	}

	public double[][] generateFinalResults() throws Exception {
		return generateFinalResults(smoothingDataSets, smoothingClassifier);
	}

	public double[][] generateFinalResults(Instances[] smoothingDataSets,
			Classifier smoothingClassifier) throws Exception {
		double[][] result = new double[smoothingDataSets[0].numInstances()][smoothingDataSets.length];
		for (int singer = 0; singer < smoothingDataSets.length; singer++) {
			for (int instance = 0; instance < result.length; instance++) {
				result[instance][singer] = smoothingClassifier
						.classifyInstance(smoothingDataSets[singer]
								.instance(instance));
			}
		}
		return result;
	}

	public Instances[] generateSmoothingDataset(double[][] assambleResults) {
		FastVector smothingAtributes = new FastVector();
		for (int w = -SMOOTHING_WINDOW_SIZE_PAST; w <= SMOOTHING_WINDOW_SIZE_FUTURE; w++) {
			smothingAtributes.addElement(new Attribute("t" + w));
		}
		smoothingDataSets = new Instances[assambleResults[0].length];
		smoothingDataSets[INDEX_MALE] = new Instances("SmothMale",
				smothingAtributes, 0);
		smoothingDataSets[INDEX_FEMALE] = new Instances("SmothFemale",
				smothingAtributes, 0);
		smoothingDataSets[INDEX_MALE].insertAttributeAt(
				assambleDataSets[INDEX_MALE].classAttribute(),
				smothingAtributes.size());
		smoothingDataSets[INDEX_FEMALE].insertAttributeAt(
				assambleDataSets[INDEX_FEMALE].classAttribute(),
				smothingAtributes.size());
		smoothingDataSets[INDEX_MALE]
				.setClassIndex(smothingAtributes.size() - 1);
		smoothingDataSets[INDEX_FEMALE]
				.setClassIndex(smothingAtributes.size() - 1);
		for (int instance = 0; instance < assambleResults.length; instance++) {
			for (int singer = 0; singer < smoothingDataSets.length; singer++) {
				Instance newInstance = new Instance(
						smothingAtributes.size() + 1);
				for (int w = -SMOOTHING_WINDOW_SIZE_PAST; w <= SMOOTHING_WINDOW_SIZE_FUTURE; w++) {
					int currentFramew = instance + w;
					if (currentFramew >= 0
							&& currentFramew < assambleResults.length)
						newInstance.setValue(w + SMOOTHING_WINDOW_SIZE_PAST,
								assambleResults[instance + w][singer]);
					else
						newInstance.setValue(w + SMOOTHING_WINDOW_SIZE_PAST, 0);
				}
				smoothingDataSets[singer].add(newInstance);
			}
		}
		return smoothingDataSets;
	}

	public Instances[] generateAssambleDatasets(Instances[] reducedDatasets,
			OperaClassifier[][] reducedClassifiers) throws Exception {
		assambleDataSets = new Instances[reducedClassifiers.length];
		for (int singer = 0; singer < reducedClassifiers.length; singer++) {
			FastVector assambleAtributes = new FastVector();
			for (int classifier = 0; classifier < reducedClassifiers[singer].length; classifier++) {
				assambleAtributes
						.addElement(reducedClassifiers[singer][classifier].atribute);
			}
			assambleAtributes.addElement(reducedDatasets[singer]
					.attribute(reducedDatasets[singer].numAttributes() - 1));
			assambleDataSets[singer] = new Instances("AssambleOpera",
					assambleAtributes, 0);
			assambleDataSets[singer].setClassIndex(assambleDataSets[singer]
					.numAttributes() - 1);
			for (int instance = 0; instance < reducedDatasets[singer]
					.numInstances(); instance++) {
				Instance newInstance = new Instance(assambleAtributes.size());
				int atributeIndex = 0;
				for (int classifier = 0; classifier < reducedClassifiers[singer].length; classifier++) {
					double result = reducedClassifiers[singer][classifier].classifier
							.classifyInstance(reducedDatasets[singer]
									.instance(instance));
					newInstance.setValue(atributeIndex++, result);
				}
				assambleDataSets[singer].add(newInstance);
			}
		}
		return assambleDataSets;
	}

	public Instances generateMultiAssambleDataset(Instances[] assambleDatasets) {
		FastVector attributes = new FastVector();
		addAttributes("", assambleDatasets[INDEX_MALE], attributes);
		addAttributes("", assambleDatasets[INDEX_FEMALE], attributes);
		FastVector classAttrValues = new FastVector();
		classAttrValues.addElement("male_only");
		classAttrValues.addElement("female_only");
		classAttrValues.addElement("both");
		classAttrValues.addElement("none");
		attributes.addElement(new Attribute("multisinger", classAttrValues));

		int numMaleAttrs = assambleDatasets[INDEX_MALE].numAttributes();
		int numFemaleAttrs = assambleDatasets[INDEX_FEMALE].numAttributes();
		int numMaleInstances = assambleDatasets[INDEX_MALE].numInstances();
		int numFemaleInstances = assambleDatasets[INDEX_FEMALE].numInstances();
		Instances result = new Instances("MultiAssamble", attributes,
				0);

		for (int i = 0; i < numMaleInstances && i < numFemaleInstances; i++) {
			Instance toAdd = new Instance(attributes.size());
			for (int attr = 0; attr < attributes.size(); attr++) {
				if (attr < numMaleAttrs - 1) {
					Instance orig = assambleDatasets[INDEX_MALE].instance(i);
					// add male attribute values
					double origValue = orig.value(attr);
					toAdd.setValue(attr, origValue);
					double setValue = toAdd.value(attr);
					if(origValue != setValue){
						System.err.println("orig and set value don't fit");
					}
				} else if (attr < numMaleAttrs + numFemaleAttrs - 2) {
					Instance orig = assambleDatasets[INDEX_FEMALE].instance(i);
					// add female attribute values
					double origValue = orig.value(attr - numMaleAttrs + 1);
					toAdd.setValue(attr, origValue);
				} else { // class attribute
					// don't set groundtruth?!

				}
				
			}

			result.add(toAdd);

		}
		return result;
	}

	private void addAttributes(String attributePrefix, Instances instances,
			FastVector attributes) {
		for (int attrIndex = 0; attrIndex < instances.numAttributes() - 1; attrIndex++) {
			String origAttrName = instances.attribute(attrIndex).name();
			attributes
					.addElement(new Attribute(attributePrefix + origAttrName));
		}
	}

	public double[][] generateAssambleResults(Instances[] assambleDatasets,
			Classifier[] assambleClassifiers) throws Exception {
		assambleResults = new double[assambleDatasets[0].numInstances()][assambleClassifiers.length];
		for (int singer = 0; singer < assambleClassifiers.length; singer++) {
			for (int instance = 0; instance < assambleDatasets[singer]
					.numInstances(); instance++) {
				assambleResults[instance][singer] = assambleClassifiers[singer]
						.classifyInstance(assambleDatasets[singer]
								.instance(instance));
			}
		}
		return assambleResults;
	}

	public Instances[] reduceDatasets(Instances rawData,
			InstanceReducer[] instanceReducers) throws Exception {
		this.reducedDatasets = new Instances[3];
		for (int i = 0; i < reducedDatasets.length; i++) {
			if (instanceReducers[i] == null)
				reducedDatasets[i] = rawData;
			else
				reducedDatasets[i] = instanceReducers[i].reduce(rawData);
		}
		return reducedDatasets;
	}

	public Instances[] reduceDataSets(Instances rawData) throws Exception {
		return reduceDatasets(rawData, instanceReducers);
	}

	public double[][] getAssambleResults() {
		return assambleResults;
	}

	public Classifier getSmoothingClassifier() {
		return smoothingClassifier;
	}

	public Instances[] getSmoothingDataSets() {
		return smoothingDataSets;
	}

	public void setAssambleClassifier(int singerIndex,
			Classifier assambleClassifier) {
		this.assambleClassifiers[singerIndex] = assambleClassifier;
	}

	public void setAssambleClassifiers(Classifier[] assambleClassifiers) {
		this.assambleClassifiers = assambleClassifiers;
	}

	public void setAssambleResults(double[][] assambleResults) {
		this.assambleResults = assambleResults;
	}

	public void setClassifiers(OperaClassifier[][] classifiers) {
		this.reducedClassifiers = classifiers;
	}

	public void setInstaceReducer(InstanceReducer[] instanceReducers) {
		this.instanceReducers = instanceReducers;
	}

	public void setInstanceReducer(int singerIndex,
			InstanceReducer instanceReducer) {
		this.instanceReducers[singerIndex] = instanceReducer;
	}

	public void setInstanceReducers(InstanceReducer[] instanceReducers) {
		this.instanceReducers = instanceReducers;
	}

	/**
	 * 
	 * @param singerIndex
	 *            INDEX_MALE, INDEX_FEMALE, INDEX_CHORUS
	 * @param classifiers
	 */
	public void setMaleClassifiers(int singerIndex,
			OperaClassifier[] classifiers) {
		reducedClassifiers[singerIndex] = classifiers;
	}

	public void setReducedClassifiers(OperaClassifier[][] reducedClassifiers) {
		this.reducedClassifiers = reducedClassifiers;
	}

	public void setSmoother(int singerIndex, Classifier smoother) {
		this.smoothingClassifier = smoother;
	}

	public void setSmoothingDataSets(Instances[] smoothingDataSets) {
		this.smoothingDataSets = smoothingDataSets;
	}

	public void setAssambleDataSets(Instances[] assambleData) {
		this.assambleDataSets = assambleData;
	}
}
