import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;

import weka.classifiers.Classifier;
import weka.core.Attribute;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;

public class LoaderUtils {
	private static final String ROOT_FOLDER = ".";

	private static final String[] FOLDERS = new String[] { "male_class",
			"female_class", "chorus_class" };

	private static final String SMOOTHING_FILE = "smoothing.model";

	private static final String TEST_DATA_FILE = "testdata.arff";

	public static OperaClassifier[][] loadClassifiers() throws Exception {
		OperaClassifier[][] arrOfLists = new OperaClassifier[3][];
		for (int folderIndex = 0; folderIndex < FOLDERS.length; folderIndex++) {
			ArrayList<OperaClassifier> arr = new ArrayList<>();
			String[] files = new File(ROOT_FOLDER, FOLDERS[folderIndex])
					.list(new FilenameFilter() {
						@Override
						public boolean accept(File dir, String name) {
							return name.endsWith("opera.model");
						}
					});
			for (String file : files) {
				Classifier cls = (Classifier) weka.core.SerializationHelper
						.read(new File(new File(ROOT_FOLDER,
								FOLDERS[folderIndex]), file).getAbsolutePath());
				OperaClassifier result = new OperaClassifier();
				result.classifier = cls;
				result.atribute = new Attribute(file);
				arr.add(result);
			}
			arrOfLists[folderIndex] = arr.toArray(new OperaClassifier[arr
					.size()]);
		}
		return arrOfLists;

	}

	public static Classifier[] loadAssembleClassifiers() throws Exception {

		ArrayList<Classifier> arr = new ArrayList<>();
		for (int folderIndex = 0; folderIndex < FOLDERS.length; folderIndex++) {
			File parent = new File(ROOT_FOLDER, FOLDERS[folderIndex]);
			String[] files = parent.list(new FilenameFilter() {

				@Override
				public boolean accept(File dir, String name) {
					return name.endsWith("assemble.model");
				}
			});
			for (String file : files) {
				Classifier cls = (Classifier) weka.core.SerializationHelper
						.read(new File(parent, file).getAbsolutePath());
				arr.add(cls);
			}
		}
		return arr.toArray(new Classifier[arr.size()]);
	}

	public static Classifier loadSmoothingClassifier() throws Exception {
		Classifier cls = (Classifier) weka.core.SerializationHelper
				.read(new File(ROOT_FOLDER, SMOOTHING_FILE).getPath());
		return cls;
	}

	public static Instances loadInstancesFile(String fileName) throws Exception {
		DataSource source = new DataSource(fileName);
		Instances data = source.getDataSet();
		return data;
	}

	public static Instances[] loadTrainInstances() throws Exception {
		Instances[] result = new Instances[3];
		for (int folderIndex = 0; folderIndex < FOLDERS.length; folderIndex++) {
			String[] files = new File(ROOT_FOLDER, FOLDERS[folderIndex])
					.list(new FilenameFilter() {

						@Override
						public boolean accept(File dir, String name) {
							return name.endsWith(".arff");
						}
					});
			for (String file : files) {
				result[folderIndex] = loadInstancesFile(new File(new File(
						ROOT_FOLDER, FOLDERS[folderIndex]), file)
						.getAbsolutePath());
			}
		}
		return result;
	}

	public static void addGroundTruth(Instances instances,
			Instances groundTruth, int gtAtributeIndex) {
		Attribute gtAtribute = groundTruth.attribute(gtAtributeIndex);
		instances.insertAttributeAt(gtAtribute, instances.numAttributes());
		int atrIndex = instances.numAttributes() - 1;
		for (int i = 0; i < instances.numInstances(); i++) {
			instances.instance(i).setValue(atrIndex,
					groundTruth.instance(i).value(gtAtributeIndex));
		}
	}

	public static void setGroundTruth(Instances instances,
			Instances groundTruth, int gtAtributeIndex) {
		int atrIndex = instances.numAttributes() - 1;
		for (int i = 0; i < instances.numInstances(); i++) {
			instances.instance(i).setValue(atrIndex,
					groundTruth.instance(i).value(gtAtributeIndex));
		}
	}

	public static void saveDataSet(Instances data, File file)
			throws IOException {
		BufferedWriter writer = new BufferedWriter(new FileWriter(file));
		writer.write(data.toString());
		writer.newLine();
		writer.flush();
		writer.close();
	}

	public static void setMultiGroundTruth(Instances multiassambleData,
			Instances gt) {
		int numAttrs = multiassambleData.numAttributes();
		for (int i = 0; i < multiassambleData.numInstances(); i++) {
			Instance toSet = multiassambleData.instance(i);
			Instance gtInstance = gt.instance(i);
			int gtMale = Math.abs((int) Math.round(gtInstance.value(gt
					.numAttributes() - MainOperaClassifier.INDEX_MALE - 1)));
			int gtFemale = Math.abs((int) Math.round(gtInstance.value(gt
					.numAttributes() - MainOperaClassifier.INDEX_FEMALE - 1)));

			// 0: male_only, 1: female_only, 2: both, 3:none
			double valToSet = 3;

			if (gtMale == 0 && gtFemale == 0) {
				valToSet = 2.0;
			} else if (gtMale == 0) {
				valToSet = 0.0;
			} else if (gtFemale == 0) {
				valToSet = 1.0;
			} else {
				valToSet = 3.0;
			}
			;
			toSet.setValue(numAttrs - 1, valToSet);
		}

	}
}
