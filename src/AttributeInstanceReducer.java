import java.util.HashSet;
import java.util.Set;

import weka.core.Attribute;
import weka.core.Instances;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Standardize;

public class AttributeInstanceReducer implements InstanceReducer {
	private Standardize filter;
	private Attribute classAttribute;
	private Set<String> atributesToKeep = new HashSet<String>();

	public AttributeInstanceReducer(Instances train) throws Exception {
		filter = new Standardize();
		filter.setInputFormat(train);
		for (int i = 0; i < train.numAttributes(); i++) {
			atributesToKeep.add(train.attribute(i).name());
		}
		classAttribute = train.attribute(train.numAttributes() - 1);
	}

	@Override
	public Instances reduce(Instances rawData) throws Exception {
		Instances instances = new Instances(rawData);
		// Instances instances = Filter.useFilter(rawData, filter);
		for (int i = 0; i < instances.numAttributes(); i++) {
			if (!atributesToKeep.contains(instances.attribute(i).name())) {
				instances.deleteAttributeAt(i);
				i--;
			}
		}
		if (instances.attribute(classAttribute.name()) == null)
			instances.insertAttributeAt(classAttribute,
					instances.numAttributes());
		instances.setClassIndex(instances.numAttributes() - 1);
		return instances;
	}
}
